Create database phone;

use phone;

CREATE TABLE numbers(
  name varchar(50),
  number varchar(30)
);

INSERT into numbers values('Primrose Chareka','902-870-6519');
INSERT into numbers values('Glenda Chiu a','416-652-1111');
INSERT into numbers values('Mark Wahlburg','613-333-2789');
